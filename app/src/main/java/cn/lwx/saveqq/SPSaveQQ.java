package cn.lwx.saveqq;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.HashMap;
import java.util.Map;

public class SPSaveQQ {
    // 保存QQ账号和登录密码到data.xml文件中
    public static boolean saveUserInfo(Context context, String number, String password) {
        //得到SharedPreferences实例 / Context上下文 / Context.MODE_PRIVATE私有模式
        SharedPreferences sp = context.getSharedPreferences("data", Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = sp.edit();//得到编辑器
        edit.putString("userName", number);//存储数据
        edit.putString("pwd", password);//存储数据
        edit.commit();//提交
        return true;
    }

    //读取操作 / 从data.xml文件中获取存储的QQ账号和密码
    public static Map<String, String> getUserInfo(Context context) {
        SharedPreferences sp = context.getSharedPreferences("data", Context.MODE_PRIVATE);
        String number = sp.getString("userName", null);//找不到数据,返回空
        String password = sp.getString("pwd", null);
        Map<String, String> userMap = new HashMap<String, String>();//存储数据
        userMap.put("number", number);
        userMap.put("password", password);
        return userMap;
    }

}