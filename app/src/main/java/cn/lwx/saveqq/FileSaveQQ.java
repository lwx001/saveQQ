package cn.lwx.saveqq;

import android.content.Context;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.Map;

public class FileSaveQQ { //工具类、保存账号、密码信息
    //保存QQ账号和登录密码到data.txt文件中
    //静态方法，通过类可直接调用。Context上下文
    public static boolean saveUserInfo(Context context, String number, String password) {
        try {
            //1、通过上下文获取文件输出流
            FileOutputStream fos = context.openFileOutput("data.txt",
                    Context.MODE_PRIVATE);//保存信息到文件中。获取文件输出流。文件名、模式
            //2、把数据写到文件中
            fos.write((number + ":" + password).getBytes());//转化为字节数组
            fos.close();//关闭流
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    //从data.txt文件中获取存储的QQ账号和密码信息
    public static Map<String, String> getUserInfo(Context context) {
        String content = "";
        try {
            FileInputStream fis = context.openFileInput("data.txt");//得到文件输入流
            byte[] buffer = new byte[fis.available()];//读取数据。缓冲区大小[fis.available()]
            fis.read(buffer);//把数据读到缓冲区中
            content = new String(buffer);
            Map<String, String> userMap = new HashMap<String, String>();
            String[] infos = content.split(":");
            userMap.put("number", infos[0]);
            userMap.put("password", infos[1]);
            fis.close();
            return userMap;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}

